const express = require('express');
const userRouter = require('./router/users')
const app = express();


app.use(express.json());
app.use(express.urlencoded({ extended: true }))


var myLogger = function (request, response, next) {
    console.log('LOGGED')
    request.time = new Date()
    next()
}

const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/baledemia', { useNewUrlParser: true, useUnifiedTopology: true })

// const db = mongoose.connection
// db.on('error', console.error.bind(console,'connection erroe:'))
// db.once('open',function(){

// })

app.use(myLogger)
// app.use(express.static('public')) //tanpa prefix
app.use(express.static('public'))
app.set('view engine', 'ejs')


app.get('/', function (request, response) {
    const kelas = {
        id: 1,
        nama: "Nodejs",
        date: request.time.toString()
    }

    response.render('pages/index', { kelas: kelas })
})

app.get('/about', function (request, response) {
    response.render('pages/about')
})

app.use(userRouter);

app.listen(3000, function () {
    console.log('server is okey')
})