const { v4: uuidv4 } = require('uuid')

const User = require('../models/user')

// let users = [
//     { id: 1, name: "Balademia", email: "bale@demia.com" },
//     { id: 2, name: "Doemi", email: "doemi@test.com" }
// ]

module.exports = {
    index: function (request, response) {
        // //metodepertama
        // User.find(function (error, data) {

        // //metodekedua
        // User.find({ email: 'agungsantoso13@gmail.com' }, function (error, data) {
        //     console.log(data)

        // //metodeketiga(bisa untuk validasi login)
        // User.findOne({ email: 'agungsantoso13@gmail.com' }, function (error, data) {
        //     console.log(data)

        // //metodekeempat
        // User.findById("6010116163cfb981c5569037", function (error, data) {
        //     console.log(data)

        //query like(pencarian)
        let keyword = {}

        if (request.query.keyword) {
            keyword = { name: { $regex: request.query.keyword } }
        }

        User.find(keyword, "name _id", function (error, data) {
            if (error) console.log(error)

            const users = data
            response.render('pages/user/index', { users })
        })

    },
    show: function (request, response) {
        const id = request.params.id
        // const data = users.filter(user => {
        //     return user.id == id
        // })
        User.findById(id, function (error, data) {
            if (error) console.log(error)

            response.render('pages/user/show', { user: data })

        })

    },
    create: function (request, response) {
        response.render('pages/user/create')
    },
    store: function (request, response) {
        // //carapertama
        // const user = new User({
        //     name: request.body.name,
        //     email: request.body.email,
        //     password: request.body.password
        // })

        // user.save(function (error, data) {
        //     if (error) console.log(error)

        //     console.log(data)
        //     response.redirect('/users')
        // })

        //carakedua
        User.create({
            name: request.body.name,
            email: request.body.email,
            password: request.body.password
        }, function (error, data) {
            if (error) console.log(error)

            console.log(data)
            response.redirect('/users')
        })

        // users.push({
        //     id: uuidv4(),
        //     name: request.body.name,
        //     email: request.body.email
        // })
    },

    update: function (request, response) {
        const id = request.params.id;
        users.filter(user => {
            if (user.id == id) {
                user.id = id,
                    user.name = request.body.name,
                    user.email = request.body.email
            }
            return user
        })
        response.json({
            status: true,
            data: users,
            message: 'Data user berhasil diupdate',
            method: request.method,
            url: request.url
        });
    },
    delete: function (request, response) {
        let id = request.params.userId
        users = users.filter(user => user.id != id)
        response.json({
            status: true,
            data: users,
            message: 'Data user berhasil dihapus',
            method: request.method,
            url: request.url
        });
    }

}